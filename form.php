﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,900&display=swap&subset=cyrillic" rel="stylesheet">
	<title> Авторизация	</title>
	<style>	
		body{
			font-family: 'Montserrat', Arial, sans-serif;
			height: 100vh;
			background-color: #FFF5F2;
			display: flex;
			justify-content: center;
			align-items: center;
		}
		.container {
			height: 500px;
			width: 500px;
			display: flex;
			justify-content: center;
			align-items: center;
			position: relative;
		}
		.form {
			width: 300px;
			padding: 32px;
			border-radius: 10px;
			box-shadow: 0 4px 16px #ccc;
			background-color: white;
			font-size: 16px;
		}
		.form_input,
		.form_button{
			font-family: 'Montserrat', Arial, sans-serif;
			font-size: 16px;
		}
		.form_title{
			text-align: center;
			margin: 0 0 32px 0;
			font-weight: normal;
		}
		.form_group{
			position: relative;
			margin-bottom: 32px;
		}
		.form_input{
			width: 100%;
			border: none;
			border-bottom: 1px solid #E0E0E0;
			background-color: transparent;
			outline: none;
		}
		.form_button{
			padding: 5px 20px;
			border: none;
			border-radius: 5px;
			color: #FFF;
			background-color: #FF6D6D;
			outline: none;
			cursor: pointer;
			transition: 0.3s;
		}
		.form_button:hover {
			background-color: #fff;
			color: #FF6D6D;
			box-shadow: 0 3px 7px #ccc;
		}

		.err {
			color: #000;
			margin: 0 auto;
			position: absolute;
			bottom: 0;
			border-bottom: 1px solid #FF6D6D;

		}

	</style>
</head>
<body>
	
	<div class="container">
		<div class="err">
			<?php
			if(isset($error))
				echo $error;
			?>
		</div>
		<form action="index.php" class="form" method="post">

			<h1 class="form_title">Авторизация</h1>

			<div class="form_group">
				<p><input class="form_input" name="login" type="text" placeholder="Введите логин" > </p>
			</div>

			<div class="form_group">
				<p><input class="form_input" name="password" type="password" placeholder="Введите пароль" ></p>
			</div>

			<input class="form_button" type="submit" value="Войти" class="button">
		</form>
	</div>
</body>
</html>